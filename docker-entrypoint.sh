#!/bin/bash

set -e
set -x

# Show env
printenv

cp -f sample_vhosts /etc/apache2/sites-available/000-default.conf
chmod 777 /etc/apache2/sites-available/000-default.conf
cp -f apache2.conf /etc/apache2/apache2.conf
chmod 777 /etc/apache2/apache2.conf
service apache2 restart

tail -f /var/log/apache2/error.log
