import React from 'react';
import { Link } from 'react-router-dom';
import ReactList from 'react-list';
import axios from 'axios';
import LazyList from 'react-list-lazy-load';
import { injector } from 'react-services-injector/index.es5';
import PreLoader from './../core/preLoader';
import ProductsSearch from './../core/productsSearch';
import ErrorPage from './errorsite';
import KodoBootstrap from './../bootstrap';

function mergePage(items, newItems, offset) {
  const merged = items.slice();
  newItems.forEach((item, idx) => {
      merged[idx + offset] = item;
    });
  return merged;
}

class ProductsList extends React.Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
    this.handleRequestPage = this.handleRequestPage.bind(this);
    this.handleSearchButton = this.handleSearchButton.bind(this);
    this.state = {
        products: [],
        productsCount: 0,
        requestSend: false,
        errorData: false,
      };
  }

  componentDidMount() {
    this.getData(0, this.props.match.params.id, (result) => {
        if (result !== false) {
          this.services.categoriesCache.setSelectedCategory(this.props.match.params.id);
          this.services.categoriesCache.setSelectedCategoryDt(this.props.match.params.dt);
          this.setState({
              errorData: false,
              requestSend: true,
              products: result.data.listing,
              productsCount: result.data.count,
            });
        } else {
          this.setState({
              requestSend: true,
              errorData: true,
            });
        }
      });
  }

  getData(page, catId, cb) {
    let productsUrl = KodoBootstrap.getBaseUrl() + `products/listing/${catId}/${page}`;

    if (this.services.searchInputCache.getSearchState()) {
      productsUrl = this.services.searchInputCache.getSearchUrl(catId, page);
    }

    this.serverRequest =
        axios
            .get(productsUrl)
            .then((result) => {
                cb(result);
              }).catch((e) => {
                cb(false);
              });
  }

  handleRequestPage (page, cb) {
    const pageSize = 32;

    this.getData(page, this.props.match.params.id, (result) => {
        if (result !== false) {
          this.setState({
              requestSend: true,
              errorData: false,
              products: mergePage(this.state.products, result.data.listing, page * pageSize),
              productsCount: result.data.count,
            });
        } else {
          this.setState({
              requestSend: true,
              errorData: true,
            });
        }

        // Tell LazyList that the page was loaded
        cb();
      });
  }

  abortRequest() {
    if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
      this.serverRequest.abort();
    }
  }

  componentWillUnmount() {
    this.abortRequest();
  }

  componentWillReceiveProps(nextProps) {
    this.abortRequest();
    this.setState({
        requestSend: false,
      });
    this.getData(0, nextProps.match.params.id, (result) => {
        if (result !== false) {
          this.services.categoriesCache.setSelectedCategory(nextProps.match.params.id);
          this.services.categoriesCache.setSelectedCategoryDt(nextProps.match.params.dt);
          this.setState({
              requestSend: true,
              errorData: false,
              products: result.data.listing,
              productsCount: result.data.count,
            });
        } else {
          this.setState({
              requestSend: true,
              errorData: true,
            });
        }
      });
  }

  handleSearchButton() {
    this.abortRequest();
    this.setState({
        requestSend: false,
      });
    this.getData(0, this.props.match.params.id, (result) => {
        if (result !== false) {
          this.setState({
              errorData: false,
              requestSend: true,
              products: result.data.listing,
              productsCount: result.data.count,
            });
        } else {
          this.setState({
              requestSend: true,
              errorData: true,
            });
        }
      });
  }

  render() {
    if (this.state.errorData) {
      return (
          <ErrorPage />
      );
    }

    if (!this.state.requestSend) {
      return (
          <main className="sub">
                    <div className="page">
                        <PreLoader />
                    </div>
                </main>
      );
    }

    const productsCount = parseInt(this.state.productsCount);
    const { products } = this.state;

    return (
        <main className="sub">
                <div className="page">
                    <div className="sub-container">
                        <h2>Wyszukiwanie produktów:</h2>
                        <ProductsSearch id={this.props.match.params.id} handleSearchButton={this.handleSearchButton} />
                        {
                      (this.state.products.length > 0) ?
                      (
                        <div className="listing-prods">
                                <LazyList
                                pageSize={32}
                                items={products}
                                length={productsCount}
                                onRequestPage={this.handleRequestPage}
                            >
                                    <ReactList
                                    type='uniform'
                                    length={productsCount}
                                    pageSize={16}
                                    itemRenderer={(index, key) => {
                                    return (
                                    products[index] !== null ? (
                                        <div key={key} className="tile product w25">
                                                <div className="inner">
                                                    <Link to={`/produkt/${products[index].id}/${products[index].domain_text}`}>
                                                        <h4>{products[index].name}</h4>
                                                        <img src={KodoBootstrap.getBaseUrl(false) + products[index].photos_paths['284x160']} alt={products[index].name}/>
                                                        {/*<span className="price">299 zł</span>*/}
                                                        <span className="button">SZCZEGÓŁY</span>
                                                    </Link>
                                                </div>
                                            </div>
                                    ) : (
                                        <div key={key} className="lazy-preloader-container">
                                                <PreLoader />
                                            </div>
                                    )
                                  );}}

                                />
                                </LazyList>
                            </div>
                      ) :
                      (
                        <div>
                              <br/>
                              <h2>BRAK PRODUKTÓW W TEJ KATEGORII</h2>
                            </div>
                      )
                    }


                    </div>
                </div>
            </main>
    );
  }
}

export default injector.connect(ProductsList);
