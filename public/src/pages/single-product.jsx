import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import axios from 'axios';
import { injector } from 'react-services-injector/index.es5';
import PreLoader from './../core/preLoader';
import ProductsSearch from './../core/productsSearch';
import ErrorPage from './errorsite';
import RawHTML from 'react-raw-html';
import Lightbox from 'react-image-lightbox';
import KodoBootstrap from './../bootstrap';

class SingleProduct extends React.Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
    this.handleSearchButton = this.handleSearchButton.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
    this.switchMainPhoto = this.switchMainPhoto.bind(this);
    this.state = {
        productData: {},
        requestSend: false,
        errorData: false,
        redirectToCat: false,
        photoIndex: 0,
        isLightboxOpen: false,
        mainPhoto: [],
      };
  }

  componentDidMount() {
    this.getData(this.props.match.params.id);
  }

  getData(id) {
    const cacheData = this.services.visitedProductsCache.getProduct(id);

    if (cacheData !== false) {
      this.setState({
          errorData: false,
          requestSend: true,
          productData: cacheData,
          redirectToCat: false,
          mainPhoto: cacheData.mainPhoto,
          photoIndex: cacheData.photoIndex,
        });
    } else {
      this.serverRequest =
          axios
              .get(KodoBootstrap.getBaseUrl() + `products/single/${id}`)
              .then((result) => {
                  this.services.visitedProductsCache.addProduct({
                      id: id,
                      data: result.data,
                    });

                  this.setState({
                      errorData: false,
                      requestSend: true,
                      productData: result.data,
                      redirectToCat: false,
                      mainPhoto: result.data.mainPhoto,
                      photoIndex: result.data.photoIndex,
                    });
                }).catch((e) => {
              this.setState({
                  requestSend: true,
                  errorData: true,
                  redirectToCat: false,
                });
            });
    }
  }

  abortRequest() {
    if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
      this.serverRequest.abort();
    }
  }

  componentWillUnmount() {
    this.abortRequest();
  }

  componentWillReceiveProps(nextProps) {
    this.abortRequest();
    this.setState({
        requestSend: false,
        redirectToCat: false,
      });
    this.getData(nextProps.match.params.id);
  }

  handleSearchButton() {
    this.abortRequest();
    this.setState({
        redirectToCat: true,
      });
  }

  openLightbox(e) {
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    e.nativeEvent.preventDefault();
    this.setState({ isLightboxOpen: true });
  }

  switchMainPhoto(index, e) {
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    e.nativeEvent.preventDefault();
    /*console.log(index);
    console.log(this.state.productData.photos);
    console.log(this.state.productData.photos[index]);*/

    this.setState({
        mainPhoto: this.state.productData.photos[index],
        photoIndex: index,
      });
  }

  render() {
    if (this.state.errorData) {
      return (
          <ErrorPage />
      );
    }

    if (!this.state.requestSend) {
      return (
          <main className="sub">
                    <div className="page">
                        <PreLoader />
                    </div>
                </main>
      );
    }

    if (this.state.redirectToCat) {
      const catId = this.services.categoriesCache.getSelectedCategory();
      const catDt = this.services.categoriesCache.getSelectedCategoryDt();

      if (catDt !== false && catId !== false) {
        return (
            <Redirect to={`/kategoria-produktow/${catId}/${catDt}`}/>
        );
      }
    }

    const photoIndex = this.state.photoIndex;
    const images = this.state.productData.photos;

    return (
        <main className="sub">
                {this.state.isLightboxOpen &&
                <Lightbox
                    mainSrc={KodoBootstrap.getBaseUrl(false) + images[photoIndex]['1366x768']}
                    nextSrc={KodoBootstrap.getBaseUrl(false) + images[(photoIndex + 1) % images.length]['1366x768']}
                    prevSrc={KodoBootstrap.getBaseUrl(false) + images[(photoIndex + images.length - 1) % images.length]['1366x768']}

                    onCloseRequest={() => this.setState({ isLightboxOpen: false })}
                    onMovePrevRequest={() => this.setState({
                            photoIndex: (photoIndex + images.length - 1) % images.length,
                          })}
                    onMoveNextRequest={() => this.setState({
                            photoIndex: (photoIndex + 1) % images.length,
                          })}
                />
            }
                <div className="page">
                    <ProductsSearch handleSearchButton={this.handleSearchButton} />
                    <div className="sub-container">
                        <div className="product-image left">
                            <a href="#" onClick={this.openLightbox} className="main-image">
                                <img src={KodoBootstrap.getBaseUrl(false) + this.state.mainPhoto['400x216']} alt={this.state.productData.product.name} />
                            </a>
                            <div className="clr"></div>
                            <div className="add-images">
                                {
                                images.map((photo, no) => (
                                    <a key={no} href="#" onClick={(e) => this.switchMainPhoto(no, e)}>
                                            <img src={KodoBootstrap.getBaseUrl(false) + photo['400x216']} alt="Zdjęcie produktu"/>
                                        </a>
                                ))
                            }
                            </div>
                        </div>
                        <div className="right product-info">
                            <h1>{this.state.productData.product.name}</h1>
                            {
                            (this.state.productData.product.prod_index.length > 0) ?
                                (
                                    <h4 className="prod-index">{this.state.productData.product.prod_index}</h4>
                                ) : null
                        }
                            {/*<span className="price"><span>Cena</span> <strong>299 zł</strong></span>*/}
                            <div className="clr"></div>
                            <div className="more-info">
                                {
                                (this.state.productData.attributes.length > 0) ?
                                    (
                                        <div>
                                                <h4>Główne cechy</h4>
                                                <ul>
                                                    {this.state.productData.attributes.map((data) => (
                                                    <li key={data.id}>{data.name}: {data.value}</li>
                                                ))}
                                                </ul>
                                            </div>
                                    ) : null
                            }
                                <RawHTML.div>
                                    {(this.state.productData.product.description) ? this.state.productData.product.description : null}
                                </RawHTML.div>
                            </div>
                        </div>
                        <div className="clr"></div>
                        {
                        (this.state.productData.connected.length > 0) ?
                            (
                                <div className="listing-prods">
                                        <h2>Produkty powiązane</h2>
                                        {
                                        this.state.productData.connected.map((item) => (
                                            <div key={item.id} className="tile product w25">
                                                    <div className="inner">
                                                        <Link to={`/produkt/${item.id}/${item.domain_text}`}>
                                                            <h4>{item.name}</h4>
                                                            <img src={KodoBootstrap.getBaseUrl(false) + item.photos_paths['284x160']} alt={item.name}/>
                                                            {/*<span className="price">299 zł</span>*/}
                                                            <span className="button">SZCZEGÓŁY</span>
                                                        </Link>
                                                    </div>
                                                </div>
                                        ))
                                    }
                                    </div>
                            ) : null
                    }
                    </div>
                </div>
            </main>
    );
  }
}

export default injector.connect(SingleProduct);
