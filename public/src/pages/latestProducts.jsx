import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { injector } from 'react-services-injector/index.es5';
import PreLoader from './../core/preLoader';
import ErrorPage from './errorsite';
import KodoBootstrap from './../bootstrap';

class NewestProducts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      requestSend: false,
      errorData: false,
    };
  }

  componentWillUnmount() {
    if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
      this.serverRequest.abort();
    }
  }

  componentDidMount() {
    const cacheData = this.services.latestProductsCache.getProducts();

    if (cacheData !== false) {
      this.setState({
        errorData: false,
        requestSend: true,
        products: cacheData,
      });
    } else {
      this.serverRequest =
       axios
        .get(KodoBootstrap.getBaseUrl() + 'products/latest')
        .then((result) => {
          this.services.latestProductsCache.setProducts(result.data.listing);

          this.setState({
            errorData: false,
            requestSend: true,
            products: result.data.listing,
          });
        }).catch((e) => {
          this.setState({
            requestSend: true,
            errorData: true,
          });
        });
    }
  }

  render() {
    if (this.state.errorData) {
      return (
       <ErrorPage />
      );
    }

    if (!this.state.requestSend) {
      return (
       <main className="sub">
					<div className="page">
						<PreLoader />
					</div>
				</main>
      );
    }

    return (
     <main id="HOME" className="sub">
				<div className="sub-container">
                    <div className="table">
                        <div className="tcell">
                            <h1>Ostatnio dodane</h1>
                            <div className="page">
                                {
                                      (this.state.products !== undefined) ?
                                      (
                                        <div>
                                          {
                                           (this.state.products.length > 0) ?
                                            (
                                             this.state.products.map((item) => (
                                              <div key={item.id} className="tile w33">
                                                <img src="/imgs/pixel-ph.png" alt=""/>
                                                <Link to={`/produkt/${item.id}/${item.domain_text}`}>
                                                  <span className="table">
                                                      <span className="tcell">
                                                          <img src={KodoBootstrap.getBaseUrl(false) + item.photos_paths['442x249']} alt={item.name} />
                                                          <h4>{item.name}</h4>
                                                          <span className="button">Przejdź</span>
                                                      </span>
                                                  </span>
                                                </Link>
                                              </div>
                                            )
                                           )) : null
                                          }
                                        </div>
                                      ) :
                                      (
                                          <div>
                                            <br/>
                                            <h2>BRAK PRODUKTÓW W KATALOGU</h2>
                                        </div>
                                      )
                                  }
                            </div>
                        </div>
                    </div>
				</div>
			</main>
    );
  }
}

export default injector.connect(NewestProducts);
