import React from 'react';
import ComponentContactList from '../core/contactlist';
import classNames from 'classnames/bind';
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import withScriptjs from 'react-google-maps/lib/async/withScriptjs';

const AsyncGettingStartedExampleGoogleMap = withScriptjs(
  withGoogleMap(
    props => (
      <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={17}
        options={
          { styles: [{ featureType: 'all', elementType: 'all', stylers: [{ saturation: -100 }, { gamma: 0.5 }] }],
}
        }
        defaultCenter={{ lat: 51.0774039, lng: 17.047800999999936 }}
        onClick={props.onMapClick}
      >
          {props.markers.map((marker, lp) => (
          <Marker
            key={lp}
            {...marker}
            onRightClick={() => props.onMarkerRightClick(marker)}
          />
        ))}
        </GoogleMap>
    )
  )
);

class ContactMain extends React.Component {
  constructor(props) {
    super(props);
    this.handleMapButton = this.handleMapButton.bind(this);
    this.state = {
        showMap: false,
      };
  }

  handleMapButton(e) {
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    e.nativeEvent.preventDefault();
    if (this.state.showMap) {
      this.setState({ showMap: false });
    } else {
      this.setState({ showMap: true });
    }

  }

  render() {
    const btnClass = classNames('button', {
        active: this.state.showMap,
      });
    const mapClass = classNames('', {
        active: this.state.showMap,
      });

    return (
      <main className="sub contact">
          <div className="sub-container">
            <div className="page">
              <div className="contact-container">
                <a href="#" className={btnClass} onClick={this.handleMapButton} id="showMap">MAPA</a>
              </div>
            </div>
            <div id="contactmain" className={mapClass}>
              <div className="tiles">
                <div className="page">
                  <h1>KONTAKT</h1>
                  <div className="left <!--w50-->">
                    <div className="adress-info black">
                      <h4>CENTRUM DRUKU</h4>
                      <ul>
                          {['Centrum Handlowe GAJ', 'ul. Świeradowska 70', 'stoiska nr 83 - 85', '50-559 Wrocław', <a href="mailto:cdoc@cdoc.pl">cdoc@cdoc.pl</a>, <a href="tel:530898383">530 89 83 83</a>].map((data, no) => <li key={no}>{data}</li>)}
                      </ul>
                    </div>
                  </div>
                  {/* <div className="right w50">
                    <div className="info">
                      <div className="input">
                        <label htmlFor="contactName">Imię i nazwisko</label>
                        <input type="text" id="contactName" placeholder="Wpisz imię i nazwisko" />
                      </div>
                      <div className="input">
                        <label htmlFor="contactMail">E-mail *</label>
                        <input required type="text" id="contactMail" placeholder="Wpisz adres e-mail" />
                      </div>
                      <div className="input">
                        <label htmlFor="contactName">Numer telefonu</label>
                        <input type="text" id="contactPhone" placeholder="Wpisz numer telefonu" />
                      </div>
                      <div className="input">
                        <label htmlFor="contactSubject">Tytuł wiadomości *</label>
                        <input required placeholder="Wpisz tytuł wiadomości" type="text" id="contactSubject" />
                      </div>
                      <div className="textarea">
                        <label htmlFor="contactContent">Treść wiadomości *</label>
                        <textarea name="" id="contactContent" placeholder="Wpisz treść wiadomości"></textarea>
                      </div>
                      <button type="submit" className="button pointer">WYŚLIJ</button>
                    </div>
                  </div> */}
                  <div className="clr"></div>
                </div>
              </div>
              <div className="clr"></div>
            </div>
            <div id="map-canvas">
              <AsyncGettingStartedExampleGoogleMap
                googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.27&libraries=places,geometry&key=AIzaSyAe27rvujWVYXpT6bDr5TgxycF8yZv4AKM"
                loadingElement={
                  <div style={{ height: `100%` }}>
                    Loading
                  </div>
                }
                containerElement={
                  <div style={{ height: `100%` }} />
                }
                mapElement={
                  <div style={{ height: `100%` }} />
                }
                onMapLoad={
                  () => {}
                }
                onMapClick={
                  () => {}
                }
                markers={[
                  {
                    position: { lat: 51.0774039, lng: 17.047800999999936 },
                    icon: '/imgs/marker.png',
                  },
                ]}
                onMarkerRightClick={
                  () => {}
                }
              />
          </div>
        </div>
      </main>
    );
  }
}

export default ContactMain;
