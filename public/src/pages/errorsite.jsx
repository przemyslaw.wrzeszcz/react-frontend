import React from 'react';
import KodoBootstrap from './../bootstrap';

export default () => (
   <main>
		<div className="table">
			<div className="tcell">
				<h1>BŁĄD 404</h1>
				<img src={KodoBootstrap.getBaseUrl(false) + '/imgs/404.jpg'} alt="404 Strona nie znaleziona"/>
				<p>Strona której szukasz nie istnieje, albo została przeniesiona</p>
			</div>
		</div>
	</main>
);
