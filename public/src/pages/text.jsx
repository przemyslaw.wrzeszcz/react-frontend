import React from 'react';
import axios from 'axios';
import ErrorPage from './errorsite';
import RawHTML from 'react-raw-html';
import { injector } from 'react-services-injector/index.es5';
import KodoBootstrap from './../bootstrap';

class TextPage extends React.Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
    this.state = {
      serverData: {
        title: null,
        content: null,
      },
      errorData: false,
    };
  }

  componentDidMount() {
    this.getData(this.props);
  }

  getData(props) {
    const cacheData = this.services.textCache.getPage(props.match.params.dt);

    if (cacheData !== false) {
      this.setState({
        errorData: false,
        serverData: cacheData,
      });
    } else {
      this.serverRequest =
       axios
        .get(KodoBootstrap.getBaseUrl() + `pages/${props.match.params.dt}`)
        .then((result) => {
          this.services.textCache.addPage({
            dt: props.match.params.dt,
            data: result.data.page,
          });

          this.setState({
            errorData: false,
            serverData: result.data.page,
          });
        }).catch((e) => {
                      this.setState({
                        errorData: true,
                      });
                    });
    }
  }

  abortRequest() {
    if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
      this.serverRequest.abort();
    }
  }

  componentWillUnmount() {
    this.abortRequest();
  }

  componentWillReceiveProps(nextProps) {
    this.abortRequest();
    this.getData(nextProps);
  }

  render() {
    if (this.state.errorData) {
      return (
       <ErrorPage />
      );
    }

    return (
     <main className="sub">
				<div className="page">
					<div className="sub-container">
						<div className="text-container">
							<h1>{(this.state.serverData.title) ? this.state.serverData.title : null}</h1>
							<div className="wyswig-content">
								<RawHTML.div>
									{(this.state.serverData.content) ? this.state.serverData.content : null}
								</RawHTML.div>
							</div>
						</div>
					</div>
				</div>
			</main>
    );
  }
}

export default injector.connect(TextPage);
