import React from 'react';
import { Link } from 'react-router-dom';

export default (props) => (
     <main id="HOME">
    	<div className="table">
			<div className="tcell">
				<h1>CENTRUM DRUKU
					<span>Tusze, tonery, komputery, akcesoria, serwis</span>
				</h1>
				<Link to="/najnowsze-produkty" className="button">Rozpocznij</Link>
			</div>
		</div>
		<div className="background background1">
			<img src="/imgs/printer-1.png" alt="Ręcznie rysowana drukarka"/>
		</div>
		<div className="background background3">
			<img src="/imgs/keyboard-1.png" alt="Ręcznie rysowana klawiatura"/>
		</div>
		<div className="background background2">
			<img src="/imgs/router-1.png" alt="Ręcznie rysowany router"/>
		</div>
    </main>
);
