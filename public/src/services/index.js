import TextCache from './textCache';
import CategoriesCache from './categoriesCache';
import SearchInputCache from './searchInputCache';
import LatestProductsCache from './latestProductsCache';
import VisitedProductsCache from './visitedProductsCache';

export default [
    {
        name: 'textCache',
        service: TextCache,
      },
    {
        name: 'categoriesCache',
        service: CategoriesCache,
      },
    {
        name: 'searchInputCache',
        service: SearchInputCache,
      },
    {
        name: 'latestProductsCache',
        service: LatestProductsCache,
      },
    {
        name: 'visitedProductsCache',
        service: VisitedProductsCache,
      },
];
