import { Service } from 'react-services-injector/index.es5';
import urlencode from 'urlencode';
import KodoBootstrap from './../bootstrap';

export default class SearchInputCache extends Service {
  constructor() {
    super();
    this.search = '';
    this.searchEnabled = false;
  }

  setSearch(data) {
    this.search = data;
  }

  setClearSearch() {
    this.search = '';
  }

  getSearch() {
    return this.search;
  }

  setSearchState(val) {
    this.searchEnabled = val;
  }

  getSearchState() {
    return this.searchEnabled;
  }

  getSearchUrl(catId, page) {
    const encoded = urlencode(this.search);
    return KodoBootstrap.getBaseUrl() + `products/listing/${catId}/${page}?search=${encoded}`;
  }
}
