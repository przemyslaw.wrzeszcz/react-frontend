import { Service } from 'react-services-injector/index.es5';

export default class TextCache extends Service {
  constructor() {
    super();
    this.pages = {};
  }

  addPage({ dt, data }) {
    this.pages[dt] = data;
  }

  getPage(dt) {
    if (Object.prototype.hasOwnProperty.call(this.pages, dt)) {
      return this.pages[dt];
    }

    return false;
  }
}
