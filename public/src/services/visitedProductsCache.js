import { Service } from 'react-services-injector/index.es5';

export default class VisitedProductsCache extends Service {
  constructor() {
    super();
    this.products = {};
  }

  addProduct({ id, data }) {
    this.products[id] = data;
  }

  getProduct(id) {
    if (Object.prototype.hasOwnProperty.call(this.products, id)) {
      return this.products[id];
    }

    return false;
  }
}
