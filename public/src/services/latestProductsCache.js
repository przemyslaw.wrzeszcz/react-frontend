import { Service } from 'react-services-injector/index.es5';

export default class LatestProductsCache extends Service {
  constructor() {
    super();
    this.products = [];
  }

  setProducts(data) {
    this.products = data;
  }

  getProducts() {
    if (this.products.length > 0) {
      return this.products;
    }

    return false;
  }
}
