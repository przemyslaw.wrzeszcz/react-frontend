import { Service } from 'react-services-injector/index.es5';

export default class CategoriesCache extends Service {
  constructor() {
    super();
    this.categories = [];
    this.selectedCategory = 0;
    this.selectedCategoryDt = '';
  }

  setCategories(categories) {
    this.categories = categories;
  }

  getCategories() {
    return (this.categories.length > 0) ? this.categories : false;
  }

  setSelectedCategory(val) {
    this.selectedCategory = val;
  }

  getSelectedCategory() {
    return (this.selectedCategory > 0) ? this.selectedCategory : false;
  }

  setSelectedCategoryDt(val) {
    this.selectedCategoryDt = val;
  }

  getSelectedCategoryDt() {
    return (this.selectedCategoryDt.length > 0) ? this.selectedCategoryDt : false;
  }
}
