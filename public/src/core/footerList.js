import React from 'react';
import { NavLink } from 'react-router-dom';

const ComponentFooterList = ({ position }) => {
  if (parseInt(position.type) === 1 && position.forward !== ':prodcats:') {
    return (
        <div>
              <NavLink exact to={position.forward} activeClassName="selected">{position.name}</NavLink>
          </div>
    );
  }

  if (parseInt(position.type) === 1 && position.forward === ':prodcats:') {
    return (
        <div>
              {position.name}
          </div>
    );
  }

  if (parseInt(position.type) === 2) {
    return (
        <div>
              <a href={position.redirect} target="_blank">{position.name}</a>
          </div>
    );
  }

  if (parseInt(position.type) === 3) {
    return (
        <div>
              <NavLink exact to={`/strony/${position.domain_text}`} activeClassName="selected">{position.name}</NavLink>
          </div>
    );
  }

  if (parseInt(position.type) === 4) {
    return (
        <div>
              <a href={`/file/${position.domain_text}`} target="_blank">{position.name}</a>
          </div>
    );
  }

  return null;
};

export default ComponentFooterList;
