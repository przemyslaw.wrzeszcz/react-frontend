import React from 'react';
import { Link } from 'react-router-dom';
import ComponentFooterList from './footerList';

class ComponentFooter extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
        <footer id="FOOTER">
            <div className="page">
              <div id="top-footer">
                <ul>
                  {
                      (this.props.menuPositions[0]) ?
                          this.props.menuPositions[0].map((position) =>
                              <li key={position.id}>
                                  <ComponentFooterList position={position} />
                              </li>
                          ) : null
                  }
                </ul>
              </div>
            </div>
            <div id="bottom-footer">
                <div className="page">
                    <p className="copyright">Developed by:
                        <a href="#" title="GeekWebsites">&nbsp;GeekWebsites</a>
                    </p>
                </div>
            </div>
        </footer>
    );
  }
};

export default ComponentFooter;
