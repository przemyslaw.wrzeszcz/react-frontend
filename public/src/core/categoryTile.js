import React from 'react';
import { Link } from 'react-router-dom';

const CategoryTile = ({ name, colour, icon, id, dt, hideOverlay }) => {
    return (
        <div key={id} className="tile color">
            <img src="/imgs/pixel-ph.png" alt=""/>
            <Link to={`/kategoria-produktow/${id}/${dt}`} className={colour} onClick={hideOverlay}>
                <span className="table">
                    <span className="tcell">
                        <i className={`icon-cdoc ${icon}`}></i>
                        {name}
                    </span>
                </span>
            </Link>
        </div>
    );
  };

export default CategoryTile;
