import React from 'react';

class PreLoader extends React.Component {
  render () {
    return (
        <div className="PreloaderContent">
                <div className="container">
                    <div className="block color1"></div>
                    <div className="block color2"></div>
                    <div className="block color3"></div>
                    <div className="block color13"></div>
                    <div className="block color1"></div>
                    <div className="block color2"></div>
                    <div className="block color3"></div>
                    <div className="block color13"></div>
                    <div className="block color1"></div>
                    <div className="block color2"></div>
                    <div className="block color3"></div>
                    <div className="block color13"></div>
                    <div className="block color1"></div>
                    <div className="block color2"></div>
                    <div className="block color3"></div>
                    <div className="block color13"></div>
                </div>
            </div>
    );
  }
}

export default PreLoader;
