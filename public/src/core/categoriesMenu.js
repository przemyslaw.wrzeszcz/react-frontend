import React from 'react';
import classNames from 'classnames/bind';
import ScrollArea from 'react-scrollbar';
import axios from 'axios';
import { injector } from 'react-services-injector/index.es5';
import CategoryTile from './categoryTile';
import KodoBootstrap from './../bootstrap';

class ComponentCategoriesMenu extends React.Component {
  constructor() {
    super();
    this.state = {
        categories: [],
      };
  }

  componentDidMount() {
    const cacheData = this.services.categoriesCache.getCategories();

    if (cacheData !== false) {
      this.setState({
          errorData: false,
          categories: cacheData,
        });
    } else {
      this.serverRequest =
          axios
              .get(KodoBootstrap.getBaseUrl() + 'categories')
              .then((result) => {
                  this.services.categoriesCache.setCategories(result.data.categories[0]);

                  this.setState({
                      categories: result.data.categories[0],
                    });
                });
    }
  }

  componentWillUnmount() {
    if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
      this.serverRequest.abort();
    }
  }

  render () {
    const btnClass = classNames(this.props.className, {
        active: this.props.pressedState,
      });

    const slicedCategories = [];

    if (this.state.categories.length > 0) {
      let i;
      let j;
      let chunk = 5;

      for (i = 0, j = this.state.categories.length; i < j; i += chunk) {
        slicedCategories.push(this.state.categories.slice(i, i + chunk));
      }
    }

    return (
        <nav id="menu-overlay" className={btnClass}>
                <ScrollArea
                speed={1.4}
                className="area gw-menu-categories-area"
                contentClassName="content gw-menu-categories"
                horizontal={true}
                vertical={false}
                swapWheelAxes={true}
                smoothScrolling={false}
            >
                    {
                    (slicedCategories.length > 0) ? slicedCategories.map((data, no) =>
                        (
                            <div key={no} className="tile-container">
                                    {data.map((row) => <CategoryTile key={row.id} id={row.id} dt={row.domain_text} hideOverlay={this.props.hideOverlay} name={row.name} colour={row.colour_class} icon={row.icon_class} />)}
                                </div>
                        )
                    ) : null
                }
                </ScrollArea>
            </nav>
    );
  }
}

export default injector.connect(ComponentCategoriesMenu);
