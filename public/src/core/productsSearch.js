import React from 'react';
import { injector } from 'react-services-injector/index.es5';
import CategoriesSelect from './categoriesSelect';

class ProductsSearch extends React.Component {
  constructor() {
    super();
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.beginTheSearch = this.beginTheSearch.bind(this);
    this.clearTheSearch = this.clearTheSearch.bind(this);
    this.state = {
        inputVal: '',
        searchEnabled: false,
      };
  }

  componentDidMount() {
    const cacheInput = this.services.searchInputCache.getSearch();
    const cacheSearch = this.services.searchInputCache.getSearchState();
    this.setState({
        inputVal: cacheInput,
        searchEnabled: cacheSearch,
      });
  }

  handleOnChange(e) {
    this.setState({
        inputVal: e.target.value,
      });
    this.services.searchInputCache.setSearch(e.target.value);
  }

  handleKeyPress(e) {
    if (e.key == 'Enter') {
      this.beginTheSearch();
    }
  }

  beginTheSearch() {
    if (this.state.inputVal.length > 0 && this.services.categoriesCache.getSelectedCategory()) {
      this.services.searchInputCache.setSearchState(true);
      this.setState({
          searchEnabled: true,
        });
      this.props.handleSearchButton();
    }
  }

  clearTheSearch() {
    this.services.searchInputCache.setSearchState(false);
    this.services.searchInputCache.setClearSearch();
    this.setState({
        inputVal: '',
        searchEnabled: false,
      });
    this.props.handleSearchButton();
  }

  render() {
    let selectedId = this.services.categoriesCache.getSelectedCategory();

    if (this.props.id !== undefined) {
      selectedId = this.props.id;
    }

    return (
        <div className="top-search-container">
                <CategoriesSelect id={selectedId} />
                <div className="input search">
                    <input type="text" name="shop" placeholder="Wyszukaj produkt" value={this.state.inputVal} onKeyPress={this.handleKeyPress} onChange={this.handleOnChange} className="active"/>
                    {
                    (this.state.searchEnabled) ?
                        (
                            <button type="button" className="clear-search" onClick={this.clearTheSearch}>X</button>
                        ) : null
                }
                    <button type="button" className="start-search" onClick={this.beginTheSearch}></button>
                </div>
            </div>
    );
  }
}

export default injector.connect(ProductsSearch);
