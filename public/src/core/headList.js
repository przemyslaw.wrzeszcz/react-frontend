import React from 'react';
import { NavLink } from 'react-router-dom';
import ComponentHeadSubmenu from './headSubmenu';

const ComponentHeadList = ({ allPositions, position, pressedState, toggleOverlay }) => {
    if (parseInt(position.type) === 1 && position.forward !== ':prodcats:') {
      return (
          <div>
                <NavLink exact to={position.forward} activeClassName="selected">{position.name}</NavLink>
                {(allPositions[position.id]) ? <ComponentHeadSubmenu allPositions={allPositions} parentId={position.id} pressedState={pressedState} toggleOverlay={toggleOverlay} /> : null}
            </div>
      );
    }

    if (parseInt(position.type) === 1 && position.forward === ':prodcats:') {
      return (
          <div>
                <a href="#" className={(pressedState) ? 'active' : ''} onClick={toggleOverlay}>{position.name}</a>
                {(allPositions[position.id]) ? <ComponentHeadSubmenu allPositions={allPositions} parentId={position.id} pressedState={pressedState} toggleOverlay={toggleOverlay} /> : null}
            </div>
      );
    }

    if (parseInt(position.type) === 2) {
      return (
          <div>
                <a href={position.redirect} target="_blank">{position.name}</a>
                {(allPositions[position.id]) ? <ComponentHeadSubmenu allPositions={allPositions} parentId={position.id} pressedState={pressedState} toggleOverlay={toggleOverlay} /> : null}
            </div>
      );
    }

    if (parseInt(position.type) === 3) {
      return (
          <div>
                <NavLink exact to={`/strony/${position.domain_text}`} activeClassName="selected">{position.name}</NavLink>
                {(allPositions[position.id]) ? <ComponentHeadSubmenu allPositions={allPositions} parentId={position.id} pressedState={pressedState} toggleOverlay={toggleOverlay} /> : null}
            </div>
      );
    }

    if (parseInt(position.type) === 4) {
      return (
          <div>
                <a href={`/file/${position.domain_text}`} target="_blank">{position.name}</a>
                {(allPositions[position.id]) ? <ComponentHeadSubmenu allPositions={allPositions} parentId={position.id} pressedState={pressedState} toggleOverlay={toggleOverlay} /> : null}
            </div>
      );
    }

    return null;
  };

export default ComponentHeadList;
