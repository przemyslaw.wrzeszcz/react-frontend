import React from 'react';

const ComponentContactList = ({ type, content }) => {
    return (
        <span className={type + 'element'}>{content}</span>
    );
  };

export default ComponentContactList;
