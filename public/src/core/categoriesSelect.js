import React from 'react';
import { injector } from 'react-services-injector/index.es5';
import { CSSTransitionGroup } from 'react-transition-group';
import { Link } from 'react-router-dom';
import onClickOutside from 'react-onclickoutside';

const CategoryListElement = (props) => {
    return (
        <span key={props.id} className="categories-list-element">
            {props.icon ? (
                <i className={`icon-cdoc ${props.icon}`}></i>
            ) : null}
            <span>{props.name}</span>
        </span>
    );
  };

class CategoriesSelect extends React.Component {
  constructor() {
    super();
    this.toggleMenu = this.toggleMenu.bind(this);
    this.getData = this.getData.bind(this);
    this.state = {
        categories: [],
        visible: false,
      };
  }

  getData() {
    var _this = this;
    const cacheData = this.services.categoriesCache.getCategories();

    if (cacheData !== false) {
      _this.setState({
          categories: cacheData,
        });
    } else {
      setTimeout(() => {
          _this.getData();
        }, 600);
    }
  }

  handleClickOutside = evt => {
      if (this.state.visible) {
        this.setState({ visible: false });
      }
    };

  componentDidMount() {
    this.getData();
  }

  toggleMenu(e) {
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    e.nativeEvent.stopPropagation();
    e.nativeEvent.preventDefault();

    if (this.state.visible) {
      this.setState({ visible: false });
    } else {
      this.setState({ visible: true });
    }
  }

  render () {
    if (this.state.categories.length <= 0) {
      return (
          <div className="shelect-container">
                    <div className="shelect-trigger" tabIndex="1">
                        <div className="shelect-option">
                            <CategoryListElement id={0} name="Ładowanie..." />
                        </div>
                    </div>
                </div>
      );
    } else {
      let selectedCat = {
          name: 'Kategorie',
          icon: null,
        };

      this.state.categories.forEach((element) => {
          if (element.id === this.props.id) {
            selectedCat.name = element.name;
            selectedCat.icon = element.icon_class;
          }
        });

      return (
          <div className="shelect-container">
                    <div className="shelect-trigger" tabIndex="1" onClick={this.toggleMenu}>
                        <div className="shelect-option">
                            <CategoryListElement id={0} name={selectedCat.name} icon={selectedCat.icon} />
                        </div>
                    </div>
                    <CSSTransitionGroup
                  transitionName="shelectmenuanim"
                  transitionEnterTimeout={300}
                  transitionLeaveTimeout={300}
              >
                        { this.state.visible ? (
                      <div key="shelect-content" className="shelect-content">
                                {
                              this.state.categories.map((row) => (
                                  <Link key={row.id} to={`/kategoria-produktow/${row.id}/${row.domain_text}`} className="shelect-option">
                                            <CategoryListElement id={row.id} name={row.name} icon={row.icon_class} />
                                        </Link>
                              ))
                          }
                            </div>
                  ) : null }
                    </CSSTransitionGroup>
                </div>
      );
    }
  }
}

export default onClickOutside(injector.connect(CategoriesSelect));
