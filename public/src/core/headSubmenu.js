import React from 'react';
import ComponentHeadList from './headList';

const ComponentHeadSubmenu = ({ allPositions, parentId, pressedState, toggleOverlay }) => {
    return (
        <div class="dropdown-menu">
    <ul>
    {
        allPositions[parentId].map((position) =>
            <li key={position.id}>
                <ComponentHeadList allPositions={allPositions} position={position} pressedState={pressedState} toggleOverlay={toggleOverlay} />
            </li>
        )
    }
    </ul>
    </div>
    );
  };

export default ComponentHeadSubmenu;
