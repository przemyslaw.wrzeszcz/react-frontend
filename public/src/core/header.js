import React from 'react';
import { Link } from 'react-router-dom';
import ComponentHeadList from './headList';
import classNames from 'classnames/bind';

class ComponentHeader extends React.Component {
  constructor() {
    super();
    this.toggleOverlay = this.toggleOverlay.bind(this);
    this.handleHeaderClick = this.handleHeaderClick.bind(this);
  }

  toggleOverlay(e) {
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    e.nativeEvent.preventDefault();
    if (this.props.mobileMenuVisible) {
      this.props.hideMobileMenu();
    }

    this.props.setIsPressed();
  }

  handleHeaderClick(e) {
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    e.nativeEvent.preventDefault();

    this.props.hideOverlay();
    this.props.hideMobileMenu();
  }

  render () {
    const menuClass = classNames('right', {
        active: this.props.mobileMenuVisible,
      });

    return (
        <header id="HEADER" onClick={this.handleHeaderClick}>
                <div className="page">
                    <Link to="/" className="logo">
                        <img src="/imgs/logo.svg" alt="Logotyp Centrum Druku"/>
                    </Link>
                    <a href="#" id="menu-trigger" className="right" onClick={this.props.toggleMobileMenu}>
                        <span></span>
                    </a>
                    <nav id="main-menu" className={menuClass}>
                        <div className="outer">
                            <div className="inner">
                                <ul>
                                    {
                                    (this.props.menuPositions[0]) ?
                                        this.props.menuPositions[0].map((position) =>
                                            <li key={position.id}>
                                                    <ComponentHeadList allPositions={this.props.menuPositions} position={position} pressedState={this.props.pressedState} toggleOverlay={this.toggleOverlay} />
                                                </li>
                                        ) : null
                                }
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
    );
  }
}

export default ComponentHeader;
