import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter, matchPath } from 'react-router-dom';
// import { CSSTransitionGroup } from 'react-transition-group';

import MainPage from './pages/main';
import LatestProducts from './pages/latestProducts';
import Text from './pages/text';
import ContactPage from './pages/contact';
import ProductListPage from './pages/products-list';
import ProductSinglePage from './pages/single-product';
import ErrorPage from './pages/errorsite';

const routes = [
    {
        path: '/',
        component: MainPage,
        exact: true,
      },
    {
        path: '/najnowsze-produkty',
        component: LatestProducts,
        exact: true,
      },
    {
        path: '/kontakt',
        component: ContactPage,
        exact: true,
      },
    {
        path: '/kategoria-produktow/:id/:dt',
        component: ProductListPage,
        exact: false,
      },
    {
        path: '/produkt/:id/:dt',
        component: ProductSinglePage,
        exact: false,
      },
    {
        path: '/strony/:dt',
        component: Text,
        exact: false,
      },
    {
        component: ErrorPage,
      },
];

class AppRoutes extends Component {
  static propTypes = {
      location: PropTypes.shape({ pathname: PropTypes.string }).isRequired,
    };

  state = {
      matchedRoutes: [],
    };

  componentWillMount() {
    this.matchRoutes(this.props.location);
  }

  componentWillReceiveProps(nextProps) {
    this.matchRoutes(nextProps.location);
  }

  matchRoutes = ({ pathname }) => {
      const matchedRoutes = [];

      for (let i = 0; i < routes.length; i += 1) {
        const { component: RouteComponent, ...rest } = routes[i];

        const match = matchPath(pathname, { ...rest });

        if (match) {
          matchedRoutes.push(
              <RouteComponent key={RouteComponent} match={match} />,
          );

          if (match.isExact || match.isStrict) {
            break;
          }
        }
      }

      this.setState({ matchedRoutes });
    };

  render() {
    return (
        <span>
                {this.state.matchedRoutes}
            </span>
    );/*
        return (
            <CSSTransitionGroup
                transitionName="pagetransition"
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}
            >
                {this.state.matchedRoutes}
            </CSSTransitionGroup>
        );*/
  }
}

export default withRouter(AppRoutes);
