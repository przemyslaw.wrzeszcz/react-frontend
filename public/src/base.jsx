import React from 'react';
import classNames from 'classnames/bind';
import axios from 'axios';
import ComponentHeader from './core/header';
import ComponentCategoriesMenu from './core/categoriesMenu';
import ComponentFooter from './core/footer';
import KodoBootstrap from './bootstrap';
import { WindowResizeListener } from 'react-window-resize-listener';

class Base extends React.Component {
  constructor() {
    super();
    this.setIsPressed = this.setIsPressed.bind(this);
    this.hideOverlay = this.hideOverlay.bind(this);
    this.toggleMobileMenu = this.toggleMobileMenu.bind(this);
    this.hideMobileMenu = this.hideMobileMenu.bind(this);
    this.state = {
        isPressed: false,
        mobileMenuVisible: false,
        menu: {
            header: {},
            footer: {},
          },
      };
  }

  componentDidMount() {
    this.serverRequest =
        axios
            .get(KodoBootstrap.getBaseUrl() + 'menu')
            .then((result) => {
                this.setState({
                    menu: result.data,
                  });
              });
  }

  componentWillUnmount() {
    if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
      this.serverRequest.abort();
    }
  }

  setIsPressed() {
    if (this.state.isPressed) {
      this.setState({ isPressed: false });
    } else {
      this.setState({ isPressed: true });
    }
  }

  hideOverlay() {
    this.setState({ isPressed: false });
  }

  toggleMobileMenu(e) {
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    e.nativeEvent.preventDefault();

    if (this.state.mobileMenuVisible) {
      this.setState({ mobileMenuVisible: false });
    } else {
      if (this.state.isPressed) {
        this.setState({ isPressed: false });
      }

      this.setState({ mobileMenuVisible: true });
    }
  }

  hideMobileMenu() {
    this.setState({ mobileMenuVisible: false });
  }

  render() {
    const btnClass = classNames(this.props.className, {
        'categories-visible': this.state.isPressed,
      });

    return (
            <div className={btnClass}>
                <WindowResizeListener onResize={windowSize => {
                        if (windowSize.windowWidth >= 700) {
                          this.hideMobileMenu();
                        }
                      }
                    }
                />
                <ComponentHeader menuPositions={this.state.menu.header} toggleMobileMenu={this.toggleMobileMenu}
                             hideMobileMenu={this.hideMobileMenu} mobileMenuVisible={this.state.mobileMenuVisible}
                             setIsPressed={this.setIsPressed} hideOverlay={this.hideOverlay}
                             pressedState={this.state.isPressed}/>
                <ComponentCategoriesMenu pressedState={this.state.isPressed} hideOverlay={this.hideOverlay}/>
                {this.props.children}
                <ComponentFooter menuPositions={this.state.menu.footer}/>
            </div>
    );
  }

}

export default Base;
