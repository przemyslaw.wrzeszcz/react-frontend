import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// import { CSSTransitionGroup } from 'react-transition-group';

import Base from './base';
import MainPage from './pages/main';
import LatestProducts from './pages/latestProducts';
import Text from './pages/text';
import ContactPage from './pages/contact';
import ProductListPage from './pages/products-list';
import ProductSinglePage from './pages/single-product';
import ErrorPage from './pages/errorsite';

/*const App = () => (
        <BrowserRouter>
                <div>
                    <Base>
                        <Route render={({ location }) => (
                            <CSSTransitionGroup
                                transitionName="pagetransition"
                                transitionEnterTimeout={300}
                                transitionLeaveTimeout={300}
                            >
                                <Switch key={location.key} location={location}>
                                    <Route path="/" exact component={MainPage}/>
                                    <Route path="/najnowsze-produkty" exact component={LatestProducts}/>
                                    <Route path="/kategoria-produktow/:id/:dt" component={ProductListPage}/>
                                    <Route path="/kontakt" exact component={ContactPage}/>
                                    <Route path="/produkt/:id/:dt" component={ProductSinglePage}/>
                                    <Route path="/strony/:dt" component={Text}/>
                                    <Route component={ErrorPage}/>
                                </Switch>
                            </CSSTransitionGroup>
                        )}/>
                    </Base>
                </div>
        </BrowserRouter>
    );*/

const App = () => (
        <BrowserRouter>
                <div>
                    <Base>
                        <Route render={({ location }) => (
                            <Switch key={location.key} location={location}>
                                <Route path="/" exact component={MainPage}/>
                                <Route path="/najnowsze-produkty" exact component={LatestProducts}/>
                                <Route path="/kategoria-produktow/:id/:dt" component={ProductListPage}/>
                                <Route path="/kontakt" exact component={ContactPage}/>
                                <Route path="/produkt/:id/:dt" component={ProductSinglePage}/>
                                <Route path="/strony/:dt" component={Text}/>
                                <Route component={ErrorPage}/>
                            </Switch>
                        )}/>
                    </Base>
                </div>
        </BrowserRouter>
    );

export default App;
