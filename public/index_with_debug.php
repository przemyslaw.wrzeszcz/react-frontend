<!DOCTYPE html>
<!--[if lt IE 9]>			<html dir="ltr" lang="pl" class="no-js ie8 lt-ie10 lt-ie9">		<![endif]-->
<!--[if IE 9]>				<html dir="ltr" lang="pl" class="no-js ie9 lt-ie10">			<![endif]-->
<!--[if gt IE 9]><!-->		<html dir="ltr" lang="pl" class="no-js">					<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>CDOC</title>
    <meta name="description" content="CDOC - Centrum druku i obróbki cyfrowej we Wrocławiu">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/cdoc-icomoon.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<!--<pre id="log"></pre>-->
<div id="root"></div>
	<!--<script type="text/javascript">
	(function () {
    var old = console.log;
    var logger = document.getElementById('log');
    console.log = function () {
      for (var i = 0; i < arguments.length; i++) {
        if (typeof arguments[i] == 'object') {
            logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
        } else {
            logger.innerHTML += arguments[i] + '<br />';
        }
      }
    }
})();
	(function () {
    var old = console.error;
    var logger = document.getElementById('log');
    console.error = function () {
      for (var i = 0; i < arguments.length; i++) {
        if (typeof arguments[i] == 'object') {
            logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
        } else {
            logger.innerHTML += arguments[i] + '<br />';
        }
      }
    }
})();
	(function () {
    var old = console.warn;
    var logger = document.getElementById('log');
    console.warn = function () {
      for (var i = 0; i < arguments.length; i++) {
        if (typeof arguments[i] == 'object') {
            logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
        } else {
            logger.innerHTML += arguments[i] + '<br />';
        }
      }
    }
})();
	
	</script>-->
<script type="text/javascript" src="/dist/bundle.js?v=8"></script>
</body>
</html>
