<?php

  function clear($path) {
      $dh = opendir($path);
  
      while ($f = readdir($dh)) {
          if ($f != '..' && $f != '.') {
              $p = $path . $f;
              if (is_dir($p)) { clear($p . '/'); rmdir($p); } else { echo $p . '<br />'; @unlink($p); }
          }
      }
      
      closedir($dh);
  }

  $cache_path = __DIR__ . '/i/';
  
  clear($cache_path);
  
?>