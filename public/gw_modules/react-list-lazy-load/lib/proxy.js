'use strict';

exports.__esModule = true;
exports.default = proxy;
var proxyMethods = ['getOffset', 'getScrollParent', 'getScroll', 'setScroll', 'getViewportSize', 'getScrollSize', 'getStartAndEnd', 'getItemSizeAndItemsPerRow', 'getSpaceBefore', 'getSizeOf', 'scrollTo', 'scrollAround', 'getVisibleRange'];

function proxy(List) {
  proxyMethods.forEach(function (name) {
    List.prototype[name] = function () {
      var _getList;
      
      return (_getList = this.getList())[name].apply(_getList, arguments);
    };
  });
  return List;
}