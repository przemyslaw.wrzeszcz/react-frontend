'use strict';

exports.__esModule = true;

var _react = require('react');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var proxyMethods = ['getOffset', 'getScrollParent', 'getScroll', 'setScroll', 'getViewportSize', 'getScrollSize', 'getStartAndEnd', 'getItemSizeAndItemsPerRow', 'getSpaceBefore', 'getSizeOf', 'scrollTo', 'scrollAround', 'getVisibleRange'];

function requestPage(call, page, cb) {
  var promise = call(page, cb);
  if (promise && promise.then) {
    promise.then(function (res) {
      return cb(null, res);
    }).then(null, cb);
  }
}

/**
 * Like `.slice`, but doesn't care about array bounds.
 *
 * [0, 1].slice(1, 3) === [1]
 * eagerSlice([0, 1], 1, 3) === [1, undefined, undefined]
 */
function eagerSlice(list, start, end) {
  var sliced = [];
  for (var i = start; i < end; i++) {
    sliced.push(list[i]);
  }
  return sliced;
}

/**
 * Adds simple lazy loading to react-list.
 */

var LazyList = function (_Component) {
  _inherits(LazyList, _Component);

  function LazyList(props) {
    _classCallCheck(this, LazyList);

    var _this = _possibleConstructorReturn(this, _Component.call(this, props));

    _this._list = null;
    _this._loadingPages = {};
    _this.updateFrame = _this.updateFrame.bind(_this);
    return _this;
  }

  LazyList.prototype.componentDidMount = function componentDidMount() {
    this.updateScrollParent();
    this.updateFrame();
  };

  LazyList.prototype.componentDidUpdate = function componentDidUpdate() {
    this.updateScrollParent();
    this.updateFrame();
  };

  LazyList.prototype.updateScrollParent = function updateScrollParent() {
    var prev = this.scrollParent;
    this.scrollParent = this.getScrollParent();
    if (prev === this.scrollParent) {
      return;
    }
    if (prev) {
      prev.removeEventListener('scroll', this.updateFrame);
    }
    if (this.props.onRequestPage) {
      this.scrollParent.addEventListener('scroll', this.updateFrame);
    }
  };

  LazyList.prototype.getList = function getList() {
    return this._list;
  };

  LazyList.prototype.isLoadingPage = function isLoadingPage(page) {
    return !!this._loadingPages[page];
  };

  LazyList.prototype.itemNeedsLoad = function itemNeedsLoad(idx) {
    var _props = this.props,
        items = _props.items,
        pageSize = _props.pageSize;

    var page = Math.floor(idx / pageSize);
    return items[idx] != null || this.isLoadingPage(page);
  };

  LazyList.prototype.updateFrame = function updateFrame() {
    var _this2 = this;

    var _props2 = this.props,
        pageSize = _props2.pageSize,
        loadMargin = _props2.loadMargin,
        items = _props2.items,
        length = _props2.length,
        onRequestPage = _props2.onRequestPage;

    // Item range that should be loaded right about now.
    if (this === null) return;
    var _getVisibleRange = this.getVisibleRange();
    if (_getVisibleRange === undefined) return;
    var topItem = _getVisibleRange[0],
        bottomItem = _getVisibleRange[1];

    if (topItem === undefined || bottomItem === undefined) {
      return;
    }

    topItem = Math.max(topItem - loadMargin, 0);
    bottomItem = Math.min(bottomItem + loadMargin, length);

    var almostVisibleItems = eagerSlice(items, topItem, bottomItem);

    var unloadedPages = almostVisibleItems.reduce(function (pages, item, idx) {
      if (item == null) {
        var page = Math.floor((topItem + idx) / pageSize);
        if (!_this2.isLoadingPage(page) && pages.indexOf(page) === -1) {
          return [].concat(pages, [page]);
        }
      }
      return pages;
    }, []);

    unloadedPages.forEach(function (page) {
      _this2._loadingPages[page] = true;
      requestPage(onRequestPage, page, function () {
        // Always delete after completion. If there was an error, we can retry
        // later. If there wasn't, we don't need to keep this around :)
        delete _this2._loadingPages[page];
      });
    });
  };

  LazyList.prototype.render = function render() {
    var _this3 = this;

    return (0, _react.cloneElement)(this.props.children, {
      ref: function ref(list) {
        _this3._list = list;
      }
    });
  };

  return LazyList;
}(_react.Component);

LazyList.propTypes = {
  /**
   * Total amount of items, on all pages.
   */
  length: _propTypes2.default.number.isRequired,
  /**
   * Items per page.
   */
  pageSize: _propTypes2.default.number,
  /**
   * When to begin loading the next page.
   */
  loadMargin: _propTypes2.default.number,
  /**
   * Loaded items. NULLs in this array indicate unloaded items.
   */
  items: _propTypes2.default.array,

  /**
   * Callback to begin loading a page.
   */
  onRequestPage: _propTypes2.default.func.isRequired
};
LazyList.defaultProps = {
  pageSize: 25,
  loadMargin: 5
};
exports.default = LazyList;


proxyMethods.forEach(function (name) {
  LazyList.prototype[name] = function () {
    var _getList;
    try {
      return (_getList = this.getList())[name].apply(_getList, arguments);
    } catch (err) {

    }
  };
});