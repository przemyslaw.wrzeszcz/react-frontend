const path = require('path');

module.exports = {
    entry: [
        "babel-polyfill",
        path.join(__dirname, '/src/main.jsx'),
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/dist',
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/, // .js .jsx
                loaders: ['babel-loader'],
                include: path.join(__dirname, '/src'),
            }, {
                // image loading and optimization
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // Images smaller than 50 kB will be compiled into base64
                            limit: 50000,
                        },
                    },
                    'image-webpack-loader',
                ],
            },
        ],
    },
};
