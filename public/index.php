<!DOCTYPE html>
<!--[if lt IE 9]>			<html dir="ltr" lang="pl" class="no-js ie8 lt-ie10 lt-ie9">		<![endif]-->
<!--[if IE 9]>				<html dir="ltr" lang="pl" class="no-js ie9 lt-ie10">			<![endif]-->
<!--[if gt IE 9]><!-->		<html dir="ltr" lang="pl" class="no-js">					<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>CDOC</title>
    <meta name="description" content="CDOC - Centrum druku i obróbki cyfrowej we Wrocławiu">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/cdoc-icomoon.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<div id="root"></div>
<script type="text/javascript" src="/dist/bundle.js?v=2"></script>
</body>
</html>
